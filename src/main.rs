use std::fs::File;
use std::io::prelude::*;

fn read_proc_tcp() -> std::io::Result<String> {
    let mut file = File::open("/proc/net/tcp")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    Ok(contents)
}

fn main() {
    match read_proc_tcp() {
        Ok(net) => {
            let v: Vec<&str> = net
                .split('\n')
                .map(|x| x.trim().split(' ').collect())
                .map(|x: Vec<&str>| if x.len() > 1 { x[5] } else { "Empty" })
                //                .filter(|&x| x != "Empty" && x != "")
                //                .map(|x| x.split(":").collect())
                //                .map(|x: Vec<&str>| if x.len() > 1 { x[1] } else { "Empty" })
                .collect();

            println!("{:?}", v)
        }
        Err(err) => println!("{:?}", err),
    }
}
